import json
import time
import os
import re
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import neptune  # pip install neptune-client

sys.path.append('../common_utils/MRCNN/common_evaluation_script_mrcnn')
from utils import utils
import utils.model as modellib
import validation_helper as helper

matplotlib.use('Agg')

# lambda function to print a line of separator in the console
print_separator = lambda x="#": print(x*100)

class NumpyEncoder(json.JSONEncoder):
    """
    Extending the josn.JSONEncoder class to customise the data before writing to json
    """
    def default(self, obj):
        """
        Ensures object to be written is numpy array 
        converts it to list before serializing
        """
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        
        # else raises a type error if input is not a numpy array saying it is not JSON serializable
        return json.JSONEncoder.default(self, obj)

def evaluate_on_images(files_=None,
                       dataset_dir_=None, 
                       save_path=None, 
                       inference_config_=None, 
                       model_path_=None,
                       prefix=None):
    """
    This function is called when we don't have the ground truth json.
    Input:
        files - list of image names to be evaluated, only jpg files are allowed
        dataset_dir - absolute path of the dataset directory where the files reside
        save_path - absolute path where the masked result images will be saved
    """
    
    assert os.path.isdir(dataset_dir), "The source directory does not exist!"
    assert save_path is not None, "save_path cannot be None!"
    
    #loading the mrcnn model in inference mode
    damage_model = helper.load_model(DAMAGE_MODEL_DIR, inference_config_, model_path_)

    print("Image to process ====>", len(files_))
    print("loading weights from model_path:", model_path_)
    # damage_model.load_weights(model_path_, by_name=True)
    # print_separator("%")    
    
    # creating the destination directory if necessary
    if not os.path.isdir(save_path):
        os.makedirs(save_path)
    if prefix:
        save_path = os.path.join(save_path, ("masked_images_"+str(prefix)))
    else:
        save_path = os.path.join(save_path, "masked_images")
    if not os.path.isdir(save_path):
        os.makedirs(save_path)
        
        
    # iterating through all the files
    prediction_count = 0
    img_count = 1
    threshold = inference_config_.DETECTION_MIN_CONFIDENCE
    for img in files_:
        sys.stdout.write("Processing {}/{}...\r".format(img_count, len(files_)))
        sys.stdout.flush()
        img_count += 1
        
        image = helper.imread(os.path.join(dataset_dir_, img))
        ##################################################################
        # prediction and post processing on a single image
        r, window, scale = helper.get_prediction_for_one_image(inference_config_, 
                                                               damage_model, 
                                                               image)
        
        pred_points_new, points_to_remove_new = helper.mask_to_polygon(r['masks'], r['class_ids'])
        del r['masks']
        r['rois'] = np.delete(r['rois'], points_to_remove_new, 0)
        r['scores'] = np.delete(r['scores'], points_to_remove_new, 0)
        r['class_ids'] = np.delete(r['class_ids'], points_to_remove_new, 0)
        r["points"] = pred_points_new
        
        # resizing the roi and polygons back to original image dim
        window = np.array(window)
        r = helper.undo_resize(window, r, scale)
        
        # Generating masks from polygons
        r['masks'], _ = helper.generate_mask_from_polygon(r['points'], 
                                                          image.shape[:2])
        ##################################################################
        
        # Updating the prediction count
        for score in r['scores']:
            if score > threshold:
                prediction_count += 1
        
        # saving the masked image
        # change the 'mode' to see add/remove details on the masked image
        helper.save_image(image,
                          os.path.join(save_path, img), 
                          np.array(r['rois']), 
                          np.array(r['masks']), 
                          np.array(r['class_ids']), 
                          np.array(r['scores']), 
                          np.array(class_names),
                          filter_classs_names=None,
                          scores_thresh=threshold,
                          mode=0)
        
    return prediction_count

def generate_data(dataset_val, inference_config_, model_path_):
    '''
    Arguments:
        dataset_val - validation dataset
        inference_config_ - configuration parameters for model inference
        model_path_ - absolute path where the trained model weights are kept
    ouput: 
        curated json files, predicted json files, raw image and resized images 
        in corresponding destination directory specified in the configuration

    steps:
        1. loads the model
        2. Creates the required directories
        3. predict damages/parts using the trained model
        4. post process the predictions
        5. save the curated json, predicted json, and raw images in the output directory
    '''
    
    #loading the mrcnn model in inference mode
    damage_model = helper.load_model(DAMAGE_MODEL_DIR, inference_config_, model_path_)

    ## ==============================Create or empty required folder=======================================
    required_folder = ['predicted', 'curated', 'raw_images', 
                       'masked_images_predicted', 'masked_images_curated']
    for folder in required_folder:
        if os.path.exists(os.path.join(output_path, folder)):
            all_files = os.listdir(os.path.join(output_path, folder))
            for file in all_files:
                os.remove(os.path.join(output_path, folder, file))
        else:
            os.makedirs(os.path.join(output_path, folder))
	## ===================================Folder created or Emptied==========================================
 
    print_separator()
    print("generating data...")
    caps_JPG = re.compile(".JPG", re.IGNORECASE) # string to be replaced
    
    # iterating through all the images in the dataset
    total_images = len(dataset_val.image_ids)
    count = 1
    for image_id in dataset_val.image_ids:
        sys.stdout.write("predicting {}/{}...\r".format(count, total_images))
        sys.stdout.flush()
        count += 1
        # print(os.path.basename(dataset_val.image_reference(image_id)))
        image_name = dataset_val.image_reference(image_id)
        
        json_name = caps_JPG.sub(".json", image_name)
        
        # read the image
        raw_image = helper.imread(image_name)
        
        # reading the image ground truth data
        gt_class_ids, gt_bbox, gt_points = helper.load_image_gt(dataset_val,
                                                              image_id)
        # reading the image
        image = dataset_val.load_image(image_id)
        
        ## ============================= Predicting and post processing ========================== 
        
        r, window, scale = helper.get_prediction_for_one_image_fusion(inference_config_, 
                                                               damage_model, 
                                                               image)
        
        pred_points, points_to_remove = helper.mask_to_polygon(r['masks'], r['class_ids'])
        del r['masks']
        r['rois'] = np.delete(r['rois'], points_to_remove, 0)
        r['scores'] = np.delete(r['scores'], points_to_remove, 0)
        r['class_ids'] = np.delete(r['class_ids'], points_to_remove, 0)
        r["points"] = pred_points
        # r['masks'] = np.delete(r['masks'], points_to_remove, 2)

        # resizing the roi and polygons back to original image dim
        window = np.array(window)
        r = helper.undo_resize(window, r, scale)
        
        r["image_location"] = os.path.basename(image_name)
        r['image_dimensions'] = (image.shape[0], image.shape[1], len(r['class_ids']))

        ##############################################################################

        ## ================================ Save predicted json ============================   
        # saving the prediction results to json file
        with open(os.path.join(output_path, "predicted", os.path.basename(json_name)), 'w') as f:
            json.dump(r, f, indent=2, cls=NumpyEncoder)
        del f  
        
        ## ================================ Save GT json ===================================
        # saving the curation details in necessary format to json file
        gt = dict()
        gt_score = []
        for _ in range(0, len(gt_class_ids)):
            gt_score.append(1)
        gt['scores'] = gt_score
        gt['rois'] = gt_bbox
        gt['class_ids'] = gt_class_ids
        gt["image_location"] = os.path.basename(image_name)
        gt['image_dimensions'] = (image.shape[0], image.shape[1], len(gt['class_ids']))
        gt['points'] = gt_points
        with open(os.path.join(output_path, "curated", os.path.basename(json_name)), 'w') as f:
            json.dump(gt, f, indent=2, cls=NumpyEncoder)
        del f

        ## ================================ Save Raw images ===================================
        helper.imsave(os.path.join(output_path, "raw_images", os.path.basename(image_name)), raw_image)
        ## =================================   Image Saved   ==================================

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description='Evaluate Mask R-CNN to for any number of parameters')
    parser.add_argument('--config', 
                        required=False,
                        default="MRCNN_config.json",    
                        help='absolute path of configuration json')
    args = parser.parse_args()
    
    # reading parameters form the passed configuration json file
    json_file = args.config
    with open(json_file) as f:
        model_data = json.load(f)
    
    # Creating inference configuration class
    Inference_config = helper.create_config_class(model_data)
    Inference_config.display()
        
    #################################################################################
    ################## reading some global parameters from the config file ##########
    config_params = dict()
    class_type = model_data['MRCNN']['class_type']
    output_path = model_data['Model_params'][class_type]['output_folder']
    thr = model_data['Model_params']['inference_config']['DETECTION_MIN_CONFIDENCE']
    DAMAGE_MODEL_DIR = model_data['Model_params'][class_type]['model_dir_log']
    DAMAGE_Images_Dir = model_data['Model_params'][class_type]['Images_Dir']
    DAMAGE_Images_SubDir = model_data['Model_params'][class_type]['Images_subDir']
    model_path = model_data['Model_params'][class_type]['model_path'] 
    IoU_threshold = float(model_data['MRCNN']['IoU_threshold'])
    class_names = model_data['Model_params'][class_type]['class_names'] 
    output_image_dir = model_data['Model_params'][class_type]['output_image_dir_name']
    test_image_only = model_data['MRCNN']['test_image_only']
    test_non_damaged_images = model_data['MRCNN']['test_non_damaged_images']
    track_on_neptune = model_data['neptune']['track_on_neptune']
    prec_recall = model_data['precision_recall']['calculate_prec_rec']
    #################################################################################
    ################## Configuring Neptune for updating the validation result #######
    #################################################################################     
    # Specify the neptune details in the MRCNN_config file
    if track_on_neptune:
        # Specify the path to the neptune configuration file 
        # It should have api_token and project_name specified
        neptune_config_file = model_data['neptune']['neptune_config']
        EXPT_ID = model_data['neptune']['expt_id']  
        
        with open(neptune_config_file) as config_file:
            neptune_config = json.load(config_file)
        
        assert 'api_token' in neptune_config.keys(), "neptune config file doesn't have 'api_token'..."
        assert 'project_name' in neptune_config.keys(), "neptune config file doesn't have 'project_name'..."
        neptune_project = neptune.Session(neptune_config["api_token"]).get_project(neptune_config["project_name"])
        expt = neptune_project.get_experiments(id=EXPT_ID)[0]
        
        # TODO - check if we are pushing to the latest expt 
        # # + are we pushing these parameters for the first time to the expt
    
    # Give a directory path here (having images and there respective .json)
    appraisers_json_folder = os.path.join(DAMAGE_Images_Dir, DAMAGE_Images_SubDir)
    # Set CSV paths and threshold - TODO change as per your local folder structure 
    output_path = os.path.join(output_path, output_image_dir)
    
    start = time.time()
    
    '''
    scenario: 
        0 - Only precision recall calculation for given thresholds - this over rides 
        1 - we need complete evaluation and the ground truth json files are available
        2 - we need evaluation but the ground truth json files are not available 
            (only NOP will be calculated and masked images will also be generated
        3 - We need only masked images
    test_image_only | test_non_damaged_images  | scenario
        False       |       False              |    1
        False       |       True               |    2
        True        |       False              |    3
        True        |       True               |    2
    '''
    scenario = None
    if prec_recall:
        scenario = 0
    elif (not test_image_only) and (not test_non_damaged_images):
        scenario = 1
    elif (not test_image_only) and test_non_damaged_images:
        scenario = 2
    elif test_image_only and (not test_non_damaged_images):
        scenario = 3
    elif test_image_only and test_non_damaged_images:
        scenario = 2
    
    if scenario == 0:
        
        # When only precision recall calculation is required.
        print_separator()
        print("Precision Recall calculation for different thresholds...\n")
        
        # Reading the relevant parameters from the config file
        from_range = model_data['precision_recall']['from_range']
        thr_range = model_data['precision_recall']['thr_range']
        thr_list = model_data['precision_recall']['thr_list']
        
        # Preparing a list of thresholds
        if from_range:
            assert len(thr_range) == 3, "Provide correct threshold range in config [min, max, step]!"
            threshold_grid = list(np.arange(thr_range[0], thr_range[1], thr_range[2]))
        else:
            threshold_grid = thr_list
        
        result_dict = {'threshold':[], 'precision':[], 'recall':[], 
                       'Fscore':[], 'EO':[], 'PO':[], 'OP':[], 'NOP':[]}
        
        print_separator()
        print("Preparing the dataset...\n")
        dataset = helper.eval_Dataset()
        dataset.load_data(DAMAGE_Images_Dir, DAMAGE_Images_SubDir, class_names, class_type)
        dataset.prepare()
        print("Images to process: {}\n\nClasses: {}\n".format(len(dataset.image_ids), dataset.class_names))
        print("Results will be saved in :{}".format(output_path))
        print_separator()
        
        for thr in threshold_grid:
            
            thr = round(thr, 2)  # rounding off to two digits to get consistant result
            Inference_config.DETECTION_MIN_CONFIDENCE = thr  # updating the config class
            
            # =============== begining of evaluation for one threshold ====================== #
            print_separator()
            print("Threshold: {}".format(thr))
            generate_data(dataset, Inference_config, model_path)
            print("Evaluating the prediction for threshold :{}".format(thr))
            eval_df = helper.evaluation(thr, class_names, output_path, IoU_threshold)
                
            # Extracting and printing final metrics 
            curated_count = np.sum(eval_df['curated_count'].values)
            overlap_count = np.sum(eval_df['overlap_count'].values)
            non_overlap_count = np.sum(eval_df['non_overlap_count'].values)
            predicted_count = np.sum(eval_df['predicted_count'].values)
            precision = overlap_count / predicted_count
            recall = overlap_count / curated_count
            fscore = 2 * precision * recall / (precision + recall)
            
            # ======================= updating the results ========================== #
            print_separator("*")
            print("Results for threshold: {}".format(thr))
            print("curated:{}, \tpredicted:{}, \tOP:{}, \tNOP:{}".format(curated_count,
                                                                         predicted_count, 
                                                                         overlap_count,
                                                                         non_overlap_count))
            print("Precision:{:.2f}, \tRecall:{:.2f}, \tF-Score:{:.2f}".format(precision * 100,
                                                                               recall * 100,
                                                                               fscore * 100))
            print_separator("*")
            
            # Appending the results to a dictionary
            result_dict['threshold'].append(thr)
            result_dict['EO'].append(curated_count)
            result_dict['PO'].append(predicted_count)
            result_dict['OP'].append(overlap_count)
            result_dict['NOP'].append(non_overlap_count)
            result_dict['precision'].append(precision)    
            result_dict['recall'].append(recall)    
            result_dict['Fscore'].append(fscore)
            # =============== end of evaluation for one threshold ====================== #
        
        # ======================= writing the results to file ========================== #
        # Writing the consolidated result to a csv file
        final_df = pd.DataFrame.from_dict(result_dict)
        out_fname = os.path.join(output_path, "precision_recall_for_thresholds.csv")
        print("Writing the precision recall results to {}".format(out_fname))
        final_df.to_csv(out_fname, header=True, index=False)
        print("*"*50)
        print(final_df)
        print("*"*50)
        end = time.time()
        print("Time taken to complete precision recall calculation is {:.2f} minutes".format((end-start)/60))
        
    elif scenario == 1:
        # General evaluation for a specific threshold
        print_separator()
        print("Preparing the dataset...\n")
        dataset = helper.eval_Dataset()
        dataset.load_data(DAMAGE_Images_Dir, DAMAGE_Images_SubDir, class_names, class_type)
        dataset.prepare()
        print("Images to process: {}\n\nClasses: {}\n".format(len(dataset.image_ids), dataset.class_names))
        print("Results will be saved in :{}".format(output_path))
        print_separator()
        
        generate_data(dataset, Inference_config, model_path)
        print_separator()
        print("Evaluating the prediction...")
        eval_df = helper.evaluation(thr, class_names, output_path, IoU_threshold)
            
        end = time.time()
        print_separator("*")
        print("Time taken to predict {} images is {:.2f} minutes".format(len(dataset.image_ids), (end-start)/60))
        print("Results are saved in :{}".format(output_path))
        
        ## ================== Extracting and printing final metrics ===============
        # Reading the results from csv and sending the results to neptune
        curated_count = np.sum(eval_df['curated_count'].values)
        overlap_count = np.sum(eval_df['overlap_count'].values)
        non_overlap_count = np.sum(eval_df['non_overlap_count'].values)
        predicted_count = np.sum(eval_df['predicted_count'].values)
        precision = overlap_count / predicted_count
        recall = overlap_count / curated_count
        fscore = 2 * precision * recall / (precision + recall)
        
        print_separator("*")
        print("curated:{}, \tpredicted:{}, \tOP:{}, \tNOP:{}".format(curated_count,
                                                                     predicted_count, 
                                                                     overlap_count,
                                                                     non_overlap_count))
        print("Precision:{:.2f}, \tRecall:{:.2f}, \tF-Score:{:.2f}".format(precision * 100,
                                                                           recall * 100,
                                                                           fscore * 100))
        print_separator("*")
        
        ## ========================= Updating neptune ========================
        # Sending the evaluation results to neptune if tracking is enabled
        # Add any additional parameters that you want to send with appropriate name
        if track_on_neptune:
            expt.set_property("curated_count", str(curated_count))
            expt.set_property("predicted_count", str(predicted_count))
            expt.set_property("overlap_count", str(overlap_count))
            expt.set_property("non_overlap_count", str(non_overlap_count))
            expt.set_property("precision", str(precision))                
            expt.set_property("recall", str(recall))                
            expt.set_property("fscore", str(fscore))
    
    elif scenario == 2:
        # When evaluation metrics is required - but all are predicted on non dent images
        print_separator()
        print("Testing only images for NOP on non dent images...")
        dataset_dir = os.path.join(DAMAGE_Images_Dir, DAMAGE_Images_SubDir)
        assert os.path.isdir(dataset_dir), "The given source directory does not exist!"
        files = os.listdir(dataset_dir)
        
        # restricting the list to only image files
        files = [item for item in files if os.path.splitext(item)[1] in ['.jpg', '.JPG']]
        assert len(files) > 0, "No images to process in the directory!"
        print("====> {} images to process.".format(len(files)))
        
        prediction_count = evaluate_on_images(files,
                                              dataset_dir, 
                                              output_path, 
                                              Inference_config,
                                              model_path, 
                                              prefix="NOP")
        
        end = time.time()
        
        # ======================= saving the results ========================== #
        # Updating the metrics and saving it in the same format
        final_df = pd.DataFrame()
        final_df['curated'] = ["curated_" + class_type]
        final_df['curated_count'] = [0]
        final_df['overlap_count'] = [0]
        final_df['predicted_count'] = [prediction_count]
        final_df['non_overlap_count'] = [prediction_count]
        final_df['precision'] = [0.0]
        final_df['recall'] = [0.0]
        final_df['Fscore'] = [0.0]
        final_df.to_csv(os.path.join(output_path, "final-NOP-score.csv"),
                        header=True,
                        index=False)
        
        print_separator("*")
        print("Results are saved in :{}".format(output_path))
        print("Time taken to predict {} images is {:.2f} minutes".format(len(files), (end-start)/60))
        print("Number of predictions (NOP): ", prediction_count)
        print_separator("*")
        
    elif scenario == 3:
        # When no evaluation metrics are required - only masked prediction images will be generated
        
        print("Testing only images....")
        dataset_dir = os.path.join(DAMAGE_Images_Dir, DAMAGE_Images_SubDir)
        assert os.path.isdir(dataset_dir), "The given source directory does not exist!"
        files = os.listdir(dataset_dir)
        
        # restricting the list to only image files
        files = [item for item in files if os.path.splitext(item)[1] in ['.jpg', '.JPG']]
        assert len(files) > 0, "No image to process in the directory!"
        
        _ = evaluate_on_images(files, 
                               dataset_dir, 
                               output_path, 
                               Inference_config, 
                               model_path)
        
        end = time.time()
        print_separator("*")
        print("Results are saved in :{}".format(output_path))
        print("time taken to predict {} images is {:.2f} minutes".format(len(files), (end-start)/60))
        print_separator("*")
    else:
        print("Incorrect configuration!!!")
