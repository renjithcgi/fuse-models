import os
import json
import sys
import colorsys
import random
from glob import glob

import cv2
import numpy as np 
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import pandas as pd

# lambda function to print a line of separator in the console
print_separator = lambda x="#": print(x*100)

def generate_mask_from_polygon(polygons, img_dim):
    '''
    Arguments:
        polygons - list of polygons (of length 'n')
        img_dim - dimension of the image (width X height)
    Returns:
        masks - a mask of dimension (width X height X n)
    
    '''
    masks = np.array([])
    count = 0
    points_to_remove_ = []
    for idx_to_rem, polygon in enumerate(polygons):
        if len(polygon) > 2:
            m1 = polygons_to_mask(list(img_dim), polygon)
            if count == 0:
                masks = np.expand_dims(m1, 2)
                count += 1
            else:
                masks = np.dstack((masks, m1))
        else:
            # print("2 point polygon detected...")
            points_to_remove_.append(idx_to_rem)
    return masks, points_to_remove_

def polygons_to_mask(img_shape, polygons):
    mask = np.ones(img_shape[:2], dtype=np.uint8)
    mask[mask < 255] = 0
    mask = Image.fromarray(mask)
    xy = list(map(tuple, polygons))
    ImageDraw.Draw(mask).polygon(xy=xy, outline=3, fill=1)
    mask = np.array(mask, dtype=bool)
    return mask

def compute_overlaps_masks(masks1, masks2):
    """Computes IoU overlaps between two sets of masks.
    masks1, masks2: [Height, Width, instances]
    """
    
    # If either set of masks is empty return empty result
    if masks1.shape[-1] == 0 or masks2.shape[-1] == 0:
        return np.zeros((masks1.shape[-1], masks2.shape[-1]))
    # flatten masks and compute their areas
    masks1 = np.reshape(masks1 > .5, (-1, masks1.shape[-1])).astype(np.float32)
    masks2 = np.reshape(masks2 > .5, (-1, masks2.shape[-1])).astype(np.float32)
    area1 = np.sum(masks1, axis=0)
    area2 = np.sum(masks2, axis=0)
    # print('area1', area1)
    # print('area2', area2)
    # intersections and union
    intersections = np.dot(masks1.T, masks2)
    # print("intersections  ", intersections)
    union = area1[:, None] + area2[None, :] - intersections
    # print("UNION ",union)
    overlaps = intersections / union
    # print('overlaps', overlaps)
    overlaps_1 = intersections / area1[:, None]
    # print("overlaps_1_before", overlaps_1)
    overlaps_1 = np.nan_to_num(overlaps_1)
    # print("overlaps_1", overlaps_1)
    overlaps_2 = intersections / area2[None, :]
    overlaps_2 = np.nan_to_num(overlaps_2)
    # print("overlaps_2", overlaps_2)
    a = np.asarray(np.maximum(overlaps, overlaps_1, overlaps_2))
    # print("a",a)
    # print("Actuals    ==============   ",a)
    return a

def trim_zeros(x):
    """It's common to have tensors larger than the available data and
    pad with zeros. This function removes rows that are all zeros.

    x: [rows, columns].
    """
    assert len(x.shape) == 2
    return x[~np.all(x == 0, axis=1)]

def compute_matches(gt_boxes, gt_class_ids, gt_masks,
                    pred_boxes, pred_class_ids, pred_scores, pred_masks,
                    iou_threshold=0.5, score_threshold=0.0, overlap_score_thr=0.0):
    """
    Finds matches between prediction and ground truth instances.

    Returns:
        gt_match: 1-D array. For each GT box it has the index of the matched
                  predicted box.
        pred_match: 1-D array. For each predicted box, it has the index of
                    the matched ground truth box.
        overlaps: [pred_boxes, gt_boxes] IoU overlaps.
    """
    # Trim zero padding
    # Keeping track of which one to keep
    to_keep = {'model1':[], 'model2':[]}
    
    # TODO: cleaner to do zero unpadding upstream
    gt_boxes = trim_zeros(gt_boxes)
    # print(gt_boxes)
    gt_masks = gt_masks[..., :gt_boxes.shape[0]]
    pred_boxes = trim_zeros(pred_boxes)
    # print(pred_boxes)
    pred_scores = pred_scores[:pred_boxes.shape[0]]
    # Sort predictions by score from high to low
    indices = np.argsort(pred_scores)[::-1]
    pred_boxes = pred_boxes[indices]
    pred_class_ids = pred_class_ids[indices]
    pred_scores = pred_scores[indices]
    # print(pred_masks)

    pred_masks = pred_masks[..., indices]

    # Compute IoU overlaps [pred_masks, gt_masks]
    overlaps = compute_overlaps_masks(pred_masks, gt_masks)

    # Loop through predictions and find matching ground truth boxes
    match_count = 0
    pred_match = -1 * np.ones([pred_boxes.shape[0]])
    gt_match = -1 * np.ones([gt_boxes.shape[0]])
    
    # print("===============",pred_match)
    # print("+++++++++++++++",gt_match)
    for i in range(len(pred_boxes)):
        # Find best matching ground truth box
        # 1. Sort matches by score
        sorted_ixs = np.argsort(overlaps[i])[::-1]
        # 2. Remove low scores
        low_score_idx = np.where(overlaps[i, sorted_ixs] < score_threshold)[0]
        if low_score_idx.size > 0:
            sorted_ixs = sorted_ixs[:low_score_idx[0]]
        # 3. Find the match
        for j in sorted_ixs:
            # If ground truth box is already matched, go to next one
            if gt_match[j] > -1:
                continue
            # If we reach IoU smaller than the threshold, end the loop
            iou = overlaps[i, j]
            if iou < iou_threshold:
                break
            # Do we have a match?
            if pred_class_ids[i] == gt_class_ids[j]:
                # using only those with score above a threshold
                if pred_scores[i] > overlap_score_thr:
                    match_count += 1
                    gt_match[j] = gt_class_ids[j]
                    pred_match[i] = pred_class_ids[i]
                    # keep the model2 results where there is overlap
                    to_keep['model2'].append(i)
                else:
                    pass
                break

    return gt_match, pred_match, overlaps, to_keep

def compute_matches_eval(gt_boxes, gt_class_ids, gt_masks,
                    pred_boxes, pred_class_ids, pred_scores, pred_masks,
                    iou_threshold=0.5, score_threshold=0.0):
    """
    Finds matches between prediction and ground truth instances.

    Returns:
        gt_match: 1-D array. For each GT box it has the index of the matched
                  predicted box.
        pred_match: 1-D array. For each predicted box, it has the index of
                    the matched ground truth box.
        overlaps: [pred_boxes, gt_boxes] IoU overlaps.
    """
    # Trim zero padding
    # Keeping track of which one to keep
    to_keep = {'model1':[], 'model2':[]}
    
    # TODO: cleaner to do zero unpadding upstream
    gt_boxes = trim_zeros(gt_boxes)
    # print(gt_boxes)
    gt_masks = gt_masks[..., :gt_boxes.shape[0]]
    pred_boxes = trim_zeros(pred_boxes)
    # print(pred_boxes)
    pred_scores = pred_scores[:pred_boxes.shape[0]]
    # Sort predictions by score from high to low
    indices = np.argsort(pred_scores)[::-1]
    pred_boxes = pred_boxes[indices]
    pred_class_ids = pred_class_ids[indices]
    pred_scores = pred_scores[indices]
    # print(pred_masks)

    pred_masks = pred_masks[..., indices]

    # Compute IoU overlaps [pred_masks, gt_masks]
    overlaps = compute_overlaps_masks(pred_masks, gt_masks)

    # Loop through predictions and find matching ground truth boxes
    match_count = 0
    pred_match = -1 * np.ones([pred_boxes.shape[0]])
    gt_match = -1 * np.ones([gt_boxes.shape[0]])
    
    # print("===============",pred_match)
    # print("+++++++++++++++",gt_match)
    for i in range(len(pred_boxes)):
        # Find best matching ground truth box
        # 1. Sort matches by score
        sorted_ixs = np.argsort(overlaps[i])[::-1]
        # 2. Remove low scores
        low_score_idx = np.where(overlaps[i, sorted_ixs] < score_threshold)[0]
        if low_score_idx.size > 0:
            sorted_ixs = sorted_ixs[:low_score_idx[0]]
        # 3. Find the match
        for j in sorted_ixs:
            # If ground truth box is already matched, go to next one
            if gt_match[j] > -1:
                continue
            # If we reach IoU smaller than the threshold, end the loop
            iou = overlaps[i, j]
            if iou < iou_threshold:
                break
            # Do we have a match?
            if pred_class_ids[i] == gt_class_ids[j]:
                match_count += 1
                gt_match[j] = gt_class_ids[j]
                pred_match[i] = pred_class_ids[i]
                # keep the model2 results where there is overlap
                to_keep['model2'].append(i)
                break

    return gt_match, pred_match, overlaps, to_keep

class NumpyEncoder(json.JSONEncoder):
    """
    Extending the josn.JSONEncoder class to customise the data before writing to json
    """
    def default(self, obj):
        """
        Ensures object to be written is numpy array 
        converts it to list before serializing
        """
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        
        # else raises a type error if input is not a numpy array saying it is not JSON serializable
        return json.JSONEncoder.default(self, obj)

def evaluation(class_names, output_path, IoU_threshold, 
               save_masked_image=True, use_only_roi_with_gt=False):
    """
    Calculates the performance metrics from the model predictions
    Masked_images_curated and masked_images_predicted are generated during this function call
    
    Inputs:
        threshold - The DETECTION_MIN_CONFIDENCE threshold for detected polygons to be counted as matching
        class_names - list of classes that needs to be predicted 
        output_path - Folder that contains predicted json, curated json and raw images
    Returns:
        final_df - Dataframe containing precision, recall, and accuracy 
                   The calculated performance metrics are returned 
                   so that it can be sent to neptune from main function
    """
    ## ================= Create empty dictionary with all class names ===============##
    class_wise_data = dict()
    
    # index starting from 1 to avoid background(BG) class
    for c in class_names[1:]:  
        class_wise_data["curated_"+str(c)] = 0
        class_wise_data["predicted_"+str(c)] = 0
        class_wise_data["overlap_curated_"+str(c)] = 0
        class_wise_data["overlap_predicted_"+str(c)] = 0
    predicted_data = dict()
    curated_data = dict()
    ## ========================= Read all files predicted ============================##
    all_files = os.listdir(output_path["predicted"])
    
    ################################################################
    # A file to store the image wise results
    # Useful when calculating positionwise metrics
    file_wise_data_fname = output_path["file_wise_data"]
    try:
        os.remove(file_wise_data_fname)
    except FileNotFoundError:
        pass
    
    ## ========================= Read all files one by one ===========================##
    file_count = 1
    total_files = len(all_files)
    for file in all_files:
        sys.stdout.write("evaluating {}/{}...\r".format(file_count, total_files))
        sys.stdout.flush()
        # initializing a dictionary for keeping track of image wise data
        file_wise_data = dict()
        file_wise_data["curated"] = 0
        file_wise_data["predicted"] = 0
        file_wise_data["overlap_polygons"] = 0
        file_wise_data["non_overlapping_polygons"] = 0
        file_wise_data["file_name"] = file.strip(".json")
        
        ##===================== Reading Curated json  =============================##
        with open(os.path.join(output_path["curated"], file)) as json_file:
            curated_data = json.load(json_file)
        
        for gt in curated_data['class_ids']:
            class_wise_data['curated_'+class_names[int(gt)]] += 1
            file_wise_data["curated"] += 1
        
        ##===================== Reading Predicted json  =========================##
        with open(os.path.join(output_path["predicted"], file)) as json_file:
            data = json.load(json_file)

        length = len(data['scores'])
        if length > 0:
            predicted_data['image_dimensions'] = data['image_dimensions']
        class_ids = []
        rois = []
        scores = []
        points = []
        
        for i in range(0, len(data['scores'])):
            class_ids.append(data['class_ids'][i])
            rois.append(data['rois'][i])
            scores.append(data['scores'][i])
            points.append(data['points'][i])
            
        predicted_data['class_ids'] = class_ids
        predicted_data['rois'] = rois
        predicted_data['scores'] = scores
        predicted_data['points'] = points
        
        # Generating masks from polygons
        curated_data['masks'], points_to_remove_ = generate_mask_from_polygon(curated_data['points'], 
                                                                              curated_data['image_dimensions'][:2])
        
        # When some some file with 2 point polygon curated encounters
        assert len(points_to_remove_) == 0, "Curated file {} has 2 point polygon!".format(file)
        
        # If there are no polygons detected
        if len(predicted_data['class_ids']) == 0:
            
            # ####################### Handling no polygon detected scenario ###########################
            # If nothing detected, updating the result files accordingly and moving to next image
            file_wise_df = pd.DataFrame([file_wise_data])
            if os.path.isfile(file_wise_data_fname):
                file_wise_df.to_csv(file_wise_data_fname, mode='a', header=False, index=False)
            else:
                file_wise_df.to_csv(file_wise_data_fname, mode='w', header=True, index=False)
        
            # =================================   Saving the masked images   =======================
            
            if save_masked_image:
                # =================================   Save Curated Masked Image   =======================
                image = imread(os.path.join(output_path["raw_images"],
                                            os.path.basename(data['image_location'])))
                save_image(image,
                           os.path.join(output_path["masked_images_curated"],
                                        os.path.basename(data['image_location'])),
                           np.array(curated_data['rois']),
                           np.array(curated_data['masks']),
                           np.array(curated_data['class_ids']),
                           np.array(curated_data['scores']),
                           np.array(class_names),
                           filter_classs_names=None,
                           scores_thresh=0.1,
                           mode=0)
                # =================================   Save Predicted Masked Image  =======================
                masked_image_name = os.path.join(output_path["masked_images_predicted"], 
                                                 os.path.basename(data['image_location']))
                imsave(masked_image_name, image)
                
            file_count += 1           
            continue
            # ####################### Completed no class detected scenario #############################
        
        # #######################  when some detection happens #############################
        # predicted_data['class_ids'] = data['class_ids']
        for pred in predicted_data['class_ids']:
            class_wise_data['predicted_'+class_names[int(pred)]] += 1
            file_wise_data['predicted'] += 1
        
        # Generating masks from polygons
        predicted_data['masks'], points_to_remove_ = generate_mask_from_polygon(predicted_data['points'], 
                                                                                predicted_data['image_dimensions'][:2])
                
        # When some some file with 2 point polygon predicted encounters
        assert len(points_to_remove_) == 0, "Predicted file {} has 2 point polygon!".format(file)
        
        # computing the match of prediction with ground truth using the IoU_threshold
        gt_match, pred_match, _, _ = compute_matches_eval(np.array(curated_data['rois']),
                                                        np.array(curated_data['class_ids']),
                                                        np.array(curated_data['masks']),
                                                        np.array(predicted_data["rois"]),
                                                        np.array(predicted_data["class_ids"]),
                                                        np.array(predicted_data["scores"]),
                                                        np.array(predicted_data['masks']),
                                                        iou_threshold=IoU_threshold)
        # updating the overlap counts
        for gt in gt_match:
            if gt > 0:
                class_wise_data['overlap_curated_'+class_names[int(gt)]] += 1
                # file_wise_data['overlap_curated'] += 1
        for pred in pred_match:
            if pred > 0:
                class_wise_data['overlap_predicted_'+class_names[int(pred)]] += 1
                file_wise_data['overlap_polygons'] += 1
        
        # =================================   Saving Masked Images   =======================
        if save_masked_image:
            # =================================   Save Predicted Masked Image   =======================
            image = imread(os.path.join(output_path["raw_images"],
                                        os.path.basename(data['image_location'])))
            save_image(image, 
                       os.path.join(output_path["masked_images_predicted"],
                                    os.path.basename(data['image_location'])), 
                       np.array(predicted_data['rois']), 
                       np.array(predicted_data['masks']),
                       np.array(predicted_data['class_ids']), 
                       np.array(predicted_data['scores']), 
                       np.array(class_names), 
                       filter_classs_names=None, 
                       scores_thresh=0.1, 
                       mode=0)
            # =================================   Save Curated Masked Image   =======================
            save_image(image, 
                       os.path.join(output_path["masked_images_curated"],
                                    os.path.basename(data['image_location'])),
                       np.array(curated_data['rois']), 
                       np.array(curated_data['masks']), 
                       np.array(curated_data['class_ids']),
                       np.array(curated_data['scores']), 
                       np.array(class_names), 
                       filter_classs_names=None, 
                       scores_thresh=0.1, 
                       mode=0)
        
        # computing the NOP values
        file_wise_data["non_overlapping_polygons"] = file_wise_data["predicted"] - file_wise_data["overlap_polygons"]
        
        
        # ==========================  Updating the file wise data in csv   =======================
        file_count += 1
        file_wise_df = pd.DataFrame([file_wise_data])
        if os.path.isfile(file_wise_data_fname):
            file_wise_df.to_csv(file_wise_data_fname, mode='a', header=False, index=False)
        else:
            file_wise_df.to_csv(file_wise_data_fname, mode='w', header=True, index=False)
    
    ## ========================= Calculating F-score ========================
    # Calculating and saving the classwise performance metrics to file
    
    df = pd.DataFrame([class_wise_data])
    df = df.transpose()
    df = df.sort_index()
    
    final_df = pd.concat([df[0:len(class_names)-1].reset_index(),\
        df[len(class_names)-1 : 2*(len(class_names)-1)].reset_index(),\
        df[2*(len(class_names)-1) : 3*(len(class_names)-1)].reset_index(),\
        df[3*(len(class_names)-1) : 4*(len(class_names)-1)].reset_index(),], axis=1)
    
    final_df.columns = ["curated", "curated_count", "curated_overlap", "curated_overlap_count", 
                        "predicted_overlap", "overlap_count", "predicted", "predicted_count"]
    final_df.drop(['curated_overlap', 'curated_overlap_count', 'predicted_overlap', 'predicted'], 
                  axis=1, inplace=True)
    final_df["non_overlap_count"] = final_df["predicted_count"] - final_df["overlap_count"]
    final_df["precision"] = final_df["overlap_count"] / final_df["predicted_count"]
    final_df["recall"] = final_df["overlap_count"] / final_df["curated_count"]
    final_df['Fscore'] = (2 * final_df["recall"] * final_df["precision"]) / (final_df["recall"] + final_df["precision"])
    
    final_df.to_csv(output_path["final-fscore"],
                    header=True, 
                    index=False)
    
    return final_df

def fuse_models(model1_prediction_path, model2_prediction_path, results_path, mrcnn_overlap_score_thr, 
                bbox_iou_thresh, use_model2_roi, model2_free_confidence, model1_free_confidence):
    '''
    Fuses two models predicted results and generates a single prediction combining the two results
    '''
    model1_files = glob(model1_prediction_path+"/*.json")
    
    total_files = len(model1_files)
    file_count = 1
    for file1 in model1_files:
        sys.stdout.write("fusing the results {}/{}...\r".format(file_count, total_files))
        sys.stdout.flush()
        file_count += 1
        file_name = os.path.basename(file1)
        
        # reading model1 predictions
        with open(file1, 'r') as f:
            model1_prediction = json.load(f)
            no_of_model1_preds = len(model1_prediction['class_ids'])
        
        # reading model2 predictions
        file2 = os.path.join(model2_prediction_path, file_name)
        with open(file2, 'r') as f:
            model2_prediction = json.load(f)
            no_of_model2_preds = len(model2_prediction['class_ids'])
        

        results_json_file = os.path.join(results_path, file_name)
        
        # Saving the model1 prediction as it is when there are not predictions from model2
        # saving a dummy file when both the predictions are empty
        if no_of_model1_preds == 0 and no_of_model2_preds == 0:
            with open(results_json_file, 'w') as f:
                json.dump(model1_prediction, f, indent=2, cls=NumpyEncoder)
            continue
        
        # Instead of model2 polygon, use model2 roi
        if use_model2_roi:
            # generate model2_points from model2_roi
            rois = model2_prediction['rois']
            points = []
            for roi in rois:
                y1, x1, y2, x2 = roi
                poly = [[x1, y1], [x2, y1], [x2, y2], [x1, y2]]
                points.append(poly)
            model2_prediction["points"] = points
        
        #######################################################################################
        # Computing the matches
        # Generating masks from polygons
        model1_prediction['masks'], points_to_remove_ = generate_mask_from_polygon(model1_prediction['points'], 
                                                                                   model1_prediction['image_dimensions'][:2])
                
        # When some some file with 2 point polygon predicted encounters
        assert len(points_to_remove_) == 0, "Model1 predicted file {} has 2 point polygon!".format(file1)
        
        # Generating masks from polygons
        model2_prediction['masks'], points_to_remove_ = generate_mask_from_polygon(model2_prediction['points'], 
                                                                                   model2_prediction['image_dimensions'][:2])
                
        # When some some file with 2 point polygon predicted encounters
        assert len(points_to_remove_) == 0, "Model2 predicted file {} has 2 point polygon!".format(file2)
        
        if no_of_model1_preds > 0 and no_of_model2_preds > 0:
            # computing the match of prediction with ground truth using the IoU_threshold
            m1_match, m2_match, _, to_keep = compute_matches(np.array(model1_prediction['rois']),
                                                             np.array(model1_prediction['class_ids']),
                                                             np.array(model1_prediction['masks']),
                                                             np.array(model2_prediction["rois"]),
                                                             np.array(model2_prediction["class_ids"]),
                                                             np.array(model2_prediction["scores"]),
                                                             np.array(model2_prediction['masks']),
                                                             iou_threshold=bbox_iou_thresh, 
                                                             overlap_score_thr=mrcnn_overlap_score_thr)
            
            # deciding what to keep when there is no overlap
            # adding all the predictions from model1 if the confidence is above thresh1
            for i, match in enumerate(m1_match):
                if match < 0:
                    if model1_prediction['scores'][i] > model1_free_confidence:
                        to_keep['model1'].append(i)
            
            # adding all the predictions from model2 if the confidence is above thresh2
            for j, match in enumerate(m2_match):
                if match < 0:
                    if model2_prediction['scores'][j] > model2_free_confidence:
                        to_keep['model2'].append(j)                
            
        elif no_of_model1_preds > 0:
            to_keep = {'model1':[], 'model2':[]}
            # adding all the predictions from model2 if the confidence is above thresh2
            for i, _ in enumerate(model1_prediction["class_ids"]):
                if model1_prediction['scores'][i] > model1_free_confidence:
                    to_keep['model1'].append(i)
                    
        elif no_of_model2_preds > 0:
            to_keep = {'model1':[], 'model2':[]}
            # adding all the predictions from model2 if the confidence is above thresh2
            for j, _ in enumerate(model2_prediction["class_ids"]):
                if model2_prediction['scores'][j] > model2_free_confidence:
                    to_keep['model2'].append(j)
        
        # Preparing the fused json to write
        fused_result = {'class_ids': [], 
                        'scores': [], 
                        'rois': [], 
                        'points': [],
                        'masks': np.ones((model1_prediction['image_dimensions'][0], 
                                           model1_prediction['image_dimensions'][1], 0))}
        
        for i in to_keep['model1']:
            fused_result['class_ids'].append(model1_prediction['class_ids'][i])
            fused_result['scores'].append(model1_prediction['scores'][i])
            fused_result['rois'].append(model1_prediction['rois'][i])
            fused_result['points'].append(model1_prediction['points'][i])
            fused_result['masks'] = np.dstack((fused_result['masks'], 
                                              model1_prediction['masks'][:, :, i])) 
            
        for i in to_keep['model2']:
            fused_result['class_ids'].append(model2_prediction['class_ids'][i])
            fused_result['scores'].append(model2_prediction['scores'][i])
            fused_result['rois'].append(model2_prediction['rois'][i])
            fused_result['points'].append(model2_prediction['points'][i])
            fused_result['masks'] = np.dstack((fused_result['masks'],
                                              model2_prediction['masks'][:, :, i]))
        
        # merging the rois/masks which are having more than 50% overlap
        overlap_iou_thresh_for_merging = 0.5
        fused_result, redo = get_new_result(fused_result, overlap_iou_thresh_for_merging)

        # iteratively merging the polygons till there are 
        # no two polygons with overlap above the specified percentage (20%)
        while redo:
            # print("********** Recalculating The Mask *************")
            fused_result, redo = get_new_result(fused_result)
        
        ##################################################################
        points, points_to_remove = mask_to_polygon(fused_result['masks'], fused_result['class_ids'])


        # removing the predictions for which no contours are detected
        fused_result['rois'] = np.delete(fused_result['rois'], points_to_remove, 0)
        fused_result['scores'] = np.delete(fused_result['scores'], points_to_remove, 0)
        fused_result['class_ids'] = np.delete(fused_result['class_ids'], points_to_remove, 0)
        fused_result['points'] = points
        
        del fused_result['masks'], model1_prediction['masks'], model2_prediction['masks']
        ###############################################################################
        fused_result['image_dimensions'] = model1_prediction['image_dimensions'] 
        fused_result['image_location'] = model1_prediction['image_location']
        
        # Saving the fused results
        fused_json_path = os.path.join(results_path, file_name)
        with open(fused_json_path, 'w') as f:
            json.dump(fused_result, f, indent=2, cls=NumpyEncoder)
        
    print("Completed fusion of the models...")

def mask_to_polygon(all_mask, class_ids_value):
    """
	Generating polygon from predicted mask
    Input:
        all_mask - stack of masks detected 
        class_id_values - corresponding class_ids
        
    Returns:
        damage_polygon_list - list of polygons generated from masks
        points_to_remove - indices of masks to be removed where no contours are detected
    """
    damage_polygon_list = []
    points_to_remove = []
    if len(class_ids_value) > 0:
        for i in range(len(class_ids_value)):
            mask_value = all_mask[:, :, i]
            
            # ensure the cv2 versions are matching - else teh below line could through error
            contours, _ = cv2.findContours(mask_value.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            
            # when multiple contours are detected
            if len(contours) > 1:
                cont = contours[0]
                for val in range(0, len(contours)-1):
                    if len(cont) < len(contours[val+1]):
                        cont = contours[val+1]
                contour_array = np.asarray(cont)
                cont_points = []
                for j in range(len(contour_array)):
                    cont_arr = np.array(contour_array[j])
                    cont_points.append((cont_arr.reshape(-1).tolist()))
                points = cont_points

                # in case of 2 point polygon
                if len(points) > 2:
                    damage_polygon_list.append(points)
                else:
                    points_to_remove.append(i)
                    
            # when single contour is detected
            elif len(contours) == 1:
                cont = contours[0]
                contour_array = np.asarray(cont)
                cont_points = []
                for j in range(len(contour_array)):
                    cont_arr = np.array(contour_array[j])
                    cont_points.append((cont_arr.reshape(-1).tolist()))
                points = cont_points
                
                # in case of 2 point polygon
                if len(points) > 2:
                    damage_polygon_list.append(points)
                else:
                    points_to_remove.append(i)
            
            # if no contours are detected
            else:
                points_to_remove.append(i)

    return damage_polygon_list, points_to_remove

def imread(image_path):
    '''
    Local version of imread. 
    To chnage the way images are being read, just change here
    '''
    return plt.imread(image_path)

def imsave(save_path, img):
    '''
    Local version of imsave
    To chnage the way images are being saved, just change here
    '''
    plt.imsave(save_path, img)

def save_image(image, image_name, boxes, masks, class_ids, scores, class_names, filter_classs_names=None,
               scores_thresh=0.1, mode=0):
    """
        image: image array
        image_name: image name
        boxes: [num_instance, (y1, x1, y2, x2, class_id)] in image coordinates.
        masks: [num_instances, height, width]
        class_ids: [num_instances]
        scores: confidence scores for each box
        class_names: list of class names of the dataset
        filter_classs_names: (optional) list of class names we want to draw
        scores_thresh: (optional) threshold of confidence scores
        save_dir: (optional) the path to store image
        mode: (optional) select the result which you want
                mode = 0 , save image with bbox,class_name,score and mask;
                mode = 1 , save image with bbox,class_name and score;
                mode = 2 , save image with class_name,score and mask;
                mode = 3 , save mask with black background;
    """
    # print("writing to ", image_name.split("/")[-1])
    mode_list = [0, 1, 2, 3]
    assert mode in mode_list, "mode's value should in mode_list %s" % str(mode_list)

    # if save_dir is None:
    #    save_dir = os.path.join(os.getcwd(), "Output")
    #    if not os.path.exists(save_dir):
    #        os.makedirs(save_dir)
    useful_mask_indices = []
    N = boxes.shape[0]
    if not N:
        print("\n*** No instances in image %s to draw *** \n" % (os.path.basename(image_name)))
        imsave(image_name, image)
        return
    else:
        assert boxes.shape[0] == masks.shape[-1] == class_ids.shape[0]

    for i in range(N):
        # filter
        class_id = class_ids[i]
        score = scores[i] if scores is not None else None
        if score is None or score < scores_thresh:
            continue

        label = class_names[class_id]
        if (filter_classs_names is not None) and (label not in filter_classs_names):
            continue

        if not np.any(boxes[i]):
            # Skip this instance. Has no bbox. Likely lost in image cropping.
            continue

        useful_mask_indices.append(i)

    if len(useful_mask_indices) == 0:
        print("\n*** No instances in image %s to draw *** \n" % (os.path.basename(image_name)))
        imsave(image_name, image)
        return

    colors = random_colors(len(useful_mask_indices))

    if mode != 3:
        masked_image = image.astype(np.uint8).copy()
    else:
        masked_image = np.zeros(image.shape).astype(np.uint8)

    if mode != 1:
        for index, value in enumerate(useful_mask_indices):
            masked_image = apply_mask(masked_image, masks[:, :, value], colors[index])

    masked_image = Image.fromarray(masked_image)

    if mode == 3:
        imsave(image_name, np.array(masked_image))
        return

    draw = ImageDraw.Draw(masked_image)
    colors = np.array(colors).astype(int) * 255

    for index, value in enumerate(useful_mask_indices):
        class_id = class_ids[value]
        score = scores[value]
        label = class_names[class_id]

        y1, x1, y2, x2 = boxes[value]
        if mode != 2:
            color = tuple(colors[index])
            draw.rectangle((x1, y1, x2, y2), outline=color)

         # Label
        # font = ImageFont.truetype('/Library/Fonts/Arial.ttf', 15)
        draw.text((x1, y1), "%s %f" % (label, score), (255, 255, 255))#, font)

    imsave(image_name, np.array(masked_image))

def random_colors(N, bright=True):
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors

def apply_mask(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                  image[:, :, c] *
                                  (1 - alpha) + alpha * color[c] * 255,
                                  image[:, :, c])
    return image

def get_new_result(r, overlap_iou_thresh=0.5):
    r_new = {}
    class_ids = []
    score = []
    rois = []
    mask_list = r['masks'].copy()
    
    redo = False
    for i in range(0, len(r['class_ids'])):
        # print("mask {}:".format(i+1))
        if i == 0:
            mask_list = np.expand_dims(r['masks'][:, :, i], 2)
            class_ids.append(r['class_ids'][i])
            score.append(r['scores'][i])
            rois.append(r['rois'][i])
        else:
            add_mask, score, mask_list, rois, total_overlap = get_new_mask(mask_list, 
                                                                           score,
                                                                           r['masks'][:, :, i],
                                                                           r['scores'][i],
                                                                           len(class_ids), 
                                                                           rois, 
                                                                           r['rois'][i], 
                                                                           overlap_iou_thresh)
            if add_mask:
                mask_list = np.dstack((mask_list, r['masks'][:, :, i]))
                class_ids.append(r['class_ids'][i])
                score.append(r['scores'][i])
                rois.append(r['rois'][i])
                
            if total_overlap > 1:
                redo = True
                # print("********** Should Recalculate The Mask *************")
                
    class_ids = np.array(class_ids)
    score = np.array(score)
    rois = np.array(rois)
    r_new = {'class_ids': class_ids, 'masks': mask_list, 'rois':rois, 'scores': score}
    
    return r_new, redo

def get_new_mask(mask_list, scores, new_mask, new_scrore, length, rois, new_roi, overlap_iou_thresh=0.5):
    add_mask = True
    overlap_thresh = overlap_iou_thresh
    total_overlap = 0
    
    for i in range(0, length):
        overlaps_both, overlap_on_mask1, overlap_on_mask2 = compute_overlaps_masks2(np.expand_dims(mask_list[:, :, i], 2), 
                                                                                   np.expand_dims(new_mask, -1))
  
        # Merging the masks if they overlap more than a threshold value (20%)
        # How did this threshold finalized?
        # print("\tOverlap with {} the element of mask_list".format(i), overlaps_both, overlap_on_mask1, overlap_on_mask2)
        if overlaps_both > overlap_thresh or overlap_on_mask1 > overlap_thresh or overlap_on_mask2 > overlap_thresh:
            total_overlap += 1
            add_mask = False
            # print("\tmerging with the {}th element of mask_list".format(i))
            # Merging the mask
            mask_list[:, :, i] = mask_list[:, :, i] + new_mask
            scores[i] = max(scores[i], new_scrore)
            
            # If masks are merged, merge roi as well
            # append the merged roi to rois
            merged_roi = [min(rois[i][0], new_roi[0]), 
                          min(rois[i][1], new_roi[1]), 
                          max(rois[i][2], new_roi[2]), 
                          max(rois[i][3], new_roi[3])]
            rois[i] = merged_roi
            
    return add_mask, scores, mask_list, rois, total_overlap        

def compute_overlaps_masks2(masks1, masks2):
    '''Computes IoU overlaps between two sets of masks for all possibility.
    masks1, masks2: these are input for computing overlap.
    mask1 and mask2 has shape (height, width, 1)
    # TODO: check how the IoU comparison is done for different classes
    '''
    # Converting the boolean mask to float32 mask with True=1, and False=0
    masks1 = np.reshape(masks1 > .5, (-1, masks1.shape[-1])).astype(np.float32)
    masks2 = np.reshape(masks2 > .5, (-1, masks2.shape[-1])).astype(np.float32)
    
    # Calculatingn the area by taking sum of pixels by column
    area1 = np.sum(masks1, axis=0)
    area2 = np.sum(masks2, axis=0)
    overlap_on_mask1 = [[0]]
    overlap_on_mask2 = [[0]]
    intersections = np.dot(masks1.T, masks2)
    union = area1[:, None] + area2[None, :] - intersections
    overlaps_both = intersections / union
    
    if intersections != 0:
        overlap_on_mask1 = intersections / area1[:, None] 
        overlap_on_mask2 = intersections / area2[:, None] 
    return overlaps_both, np.array(overlap_on_mask1), np.array(overlap_on_mask2)

if __name__ == "__main__":
    curated_path = "/home/cgiuser/renjith/fuse-models/mrcnn_evaluation_script/outputs/dent-modelv7-cms-0.5/curated"
    images_path = "/home/cgiuser/renjith/dent_validation_data_cms"

    # Assuming model 1 is better at FP suppression
    model1_prediction_path = "/home/cgiuser/renjith/ds-sandbox-repo/yolo_eval/outputs/CMS_data_0.3-v4-final/predicted"
    model2_prediction_path = "/home/cgiuser/renjith/fuse-models/mrcnn_evaluation_script/outputs/dent-modelv7-cms-0.5/predicted"
    ouput_path = "./outputs-fused-model"
    
    # curated_path = "/home/cgiuser/renjith/fuse-models/mrcnn_evaluation_script/outputs/dent-modelv7-old-0.5/curated"
    # images_path = "/home/cgiuser/renjith/ds-dent-detection/data/validation_data"

    # # Assuming model 1 is better at FP suppression
    # model1_prediction_path = "/home/cgiuser/renjith/ds-sandbox-repo/yolo_eval/outputs/Old_data_0.3-v4/predicted"
    # model2_prediction_path = "/home/cgiuser/renjith/fuse-models/mrcnn_evaluation_script/outputs/dent-modelv7-old-0.5/predicted"
    # ouput_path = "./old-outputs-fused-model"
    
    if not os.path.isdir(ouput_path):
        os.makedirs(ouput_path)

    results_path = os.path.join(ouput_path, 'fused_result')

    if not os.path.isdir(results_path):
        os.makedirs(results_path)
    
    # print("results_path: ", results_path)
    
    #=============================================================================================#    
    ################################### Fusion parameters #########################################
    # model1 -> yolo
    # model2 -> mrcnn
    
    bbox_iou_thresh = 0.2  # iou above which it is considered as overlap
    use_model2_roi = False  # To use model 2 roi or polygon for iou calculation
    # Confidence above which model2 prediction will be accepted without support from model1
    model1_free_confidence = 2     
    model2_free_confidence = 0.99
    
    mrcnn_overlap_score_thr = 0.5
    
    #TODO: While comparing with the GT, always use roi instead of polygon
    use_only_roi_with_gt = False
    # If False, use roi only when the number of points in the polygon equals to 4
    
    #=================================  fusing the mdoels ========================================#    
    print("Fusing the two models...")
    fuse_models(model1_prediction_path, model2_prediction_path, results_path, mrcnn_overlap_score_thr,  
                bbox_iou_thresh, use_model2_roi, model2_free_confidence, model1_free_confidence)
        
    ###############################################################################################    
    #============================= Evaluating the fused model ====================================#    
    output_paths = {'curated': curated_path, 
                    'predicted': results_path, 
                    'masked_images_predicted': os.path.join(ouput_path, 'masked_images_predicted'),
                    'masked_images_curated': os.path.join(ouput_path, 'masked_images_curated'), 
                    'raw_images': images_path, 
                    'final-fscore': os.path.join(ouput_path, 'final-fscore.csv'), 
                    'file_wise_data': os.path.join(ouput_path, 'file_wise_data.csv')}
    
    class_names = ['BG', 'dent']
    IoU_threshold = 0.1
    
    if not os.path.isdir(output_paths['masked_images_predicted']):
        os.makedirs(output_paths['masked_images_predicted'])
        
    if not os.path.isdir(output_paths['masked_images_curated']):
        os.makedirs(output_paths['masked_images_curated'])
    
    print("Evaluating the fused results...")
    
    eval_df = evaluation(class_names, output_paths, IoU_threshold, 
                         True, use_only_roi_with_gt)
    
    print("Results are saved in :{}".format(ouput_path))
        
    ## ================== Extracting and printing final metrics ===============
    # Reading the results from csv and sending the results to neptune
    curated_count = np.sum(eval_df['curated_count'].values)
    overlap_count = np.sum(eval_df['overlap_count'].values)
    non_overlap_count = np.sum(eval_df['non_overlap_count'].values)
    predicted_count = np.sum(eval_df['predicted_count'].values)
    precision = overlap_count / predicted_count
    recall = overlap_count / curated_count
    fscore = 2 * precision * recall / (precision + recall)
    
    print_separator("*")
    print("curated:{}, \tpredicted:{}, \tOP:{}, \tNOP:{}".format(curated_count,
                                                                 predicted_count, 
                                                                 overlap_count,
                                                                 non_overlap_count))
    print("Precision:{:.2f}, \tRecall:{:.2f}, \tF-Score:{:.2f}".format(precision * 100,
                                                                       recall * 100,
                                                                       fscore * 100))
    print_separator("*")
        